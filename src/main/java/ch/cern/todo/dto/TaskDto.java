package ch.cern.todo.dto;

import ch.cern.todo.entity.Task;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class TaskDto {
    private Long taskId;
    private String taskName;
    private String taskDescription;
    private Date deadline;
    private TaskCategoryDto taskCategory;

    public static TaskDto fromTask(Task task) {
        TaskDto taskDto = new TaskDto();

        taskDto.taskId = task.getTaskId();
        taskDto.taskName = task.getTaskName();
        taskDto.taskDescription = task.getTaskDescription();
        taskDto.deadline = task.getDeadline();
        if (task.getTaskCategory() != null) {
            taskDto.taskCategory = TaskCategoryDto.fromTaskCategory(task.getTaskCategory());
        }

        return taskDto;
    }
}
