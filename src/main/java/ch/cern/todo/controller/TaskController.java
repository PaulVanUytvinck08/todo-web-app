package ch.cern.todo.controller;

import ch.cern.todo.dto.TaskDto;
import ch.cern.todo.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/task")
public class TaskController {

    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("{taskId}")
    public TaskDto getTask(@PathVariable(value = "taskId") String taskId) {
        return this.taskService.getTask(Long.parseLong(taskId));
    }

    @GetMapping
    public List<TaskDto> getTasks() {
        return this.taskService.getTasks();
    }

    @DeleteMapping("{taskId}")
    public void deleteTask(@PathVariable(value = "taskId") String taskId) {
        this.taskService.deleteTask(Long.parseLong(taskId));
    }

    @PutMapping("{taskId}")
    public TaskDto updateTask(@PathVariable(value = "taskId") String taskId, @RequestBody TaskDto taskDto) {
        return this.taskService.updateTask(Long.parseLong(taskId),taskDto);
    }

    @PostMapping
    public TaskDto createTask(@RequestBody TaskDto taskDto) {
        return TaskDto.fromTask(this.taskService.createTask(taskDto));
    }

}
