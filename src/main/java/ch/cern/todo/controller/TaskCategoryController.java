package ch.cern.todo.controller;

import ch.cern.todo.dto.TaskCategoryDto;
import ch.cern.todo.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/task/category")
public class TaskCategoryController {
    private final TaskService taskService;

    @Autowired
    public TaskCategoryController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("{categoryId}")
    public TaskCategoryDto getTaskCategory(@PathVariable(value = "categoryId") String categoryId) {
        return this.taskService.getTaskCategory(Long.parseLong(categoryId));
    }

    @GetMapping
    public List<TaskCategoryDto> getTaskCategories() {
        return this.taskService.getTaskCategories();
    }

    @DeleteMapping("{categoryId}")
    public void deleteTaskCategory(@PathVariable(value = "categoryId") String categoryId) {
        this.taskService.deleteTaskCategory(Long.parseLong(categoryId));
    }

    @PutMapping("{categoryId}")
    public TaskCategoryDto updateTaskCategory(@PathVariable(value = "categoryId") String categoryId, @RequestBody TaskCategoryDto taskCategoryDto) {
        return this.taskService.updateTaskCategory(Long.parseLong(categoryId), taskCategoryDto);
    }

    @PostMapping
    public TaskCategoryDto createTaskCategory(@RequestBody TaskCategoryDto taskCategoryDto) {
        return TaskCategoryDto.fromTaskCategory(this.taskService.createTaskCategory(taskCategoryDto));
    }
}
