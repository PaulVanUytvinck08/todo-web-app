package ch.cern.todo.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "TASK_CATEGORY")
public class TaskCategory {
    @Id
    @SequenceGenerator(name = "TaskSequence", sequenceName = "TASK_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TaskSequence")
    @Column(name = "CATEGORY_ID", updatable = false, nullable = false)
    private Long categoryId;
    @Column(name = "CATEGORY_NAME", unique = true)
    private String categoryName;
    @Column(name = "CATEGORY_DESCRIPTION")
    private String categoryDescription;
}
