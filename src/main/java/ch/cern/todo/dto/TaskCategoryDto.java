package ch.cern.todo.dto;

import ch.cern.todo.entity.TaskCategory;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class TaskCategoryDto {
    private Long categoryId;
    private String categoryName;
    private String categoryDescription;

    public static TaskCategoryDto fromTaskCategory(TaskCategory taskCategory) {
        TaskCategoryDto taskCategoryDto = new TaskCategoryDto();

        taskCategoryDto.categoryId = taskCategory.getCategoryId();
        taskCategoryDto.categoryName = taskCategory.getCategoryName();
        taskCategoryDto.categoryDescription = taskCategory.getCategoryDescription();

        return taskCategoryDto;
    }
}
