package ch.cern.todo.service;

import ch.cern.todo.dto.TaskCategoryDto;
import ch.cern.todo.dto.TaskDto;
import ch.cern.todo.entity.Task;
import ch.cern.todo.entity.TaskCategory;

import java.util.List;

public interface TaskService {
    Task createTask(TaskDto taskDto);

    TaskDto updateTask(Long taskId, TaskDto taskDto);

    void deleteTask(Long taskId);

    List<TaskDto> getTasks();

    TaskDto getTask(Long taskId);

    TaskCategoryDto getTaskCategory(Long categoryId);

    List<TaskCategoryDto> getTaskCategories();

    void deleteTaskCategory(Long categoryId);

    TaskCategoryDto updateTaskCategory(Long categoryId, TaskCategoryDto taskCategoryDto);

    TaskCategory createTaskCategory(TaskCategoryDto taskCategoryDto);
}
