package ch.cern.todo.repository;

import ch.cern.todo.entity.TaskCategory;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TaskCategoryRepository extends CrudRepository<TaskCategory, Long> {

    Optional<TaskCategory> findTaskCategoryByCategoryName(String categoryName);
}
