package ch.cern.todo.service;

import ch.cern.todo.dto.TaskCategoryDto;
import ch.cern.todo.dto.TaskDto;
import ch.cern.todo.entity.Task;
import ch.cern.todo.entity.TaskCategory;
import ch.cern.todo.repository.TaskCategoryRepository;
import ch.cern.todo.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;
    private final TaskCategoryRepository taskCategoryRepository;

    @Autowired
    public TaskServiceImpl(TaskRepository taskRepository, TaskCategoryRepository taskCategoryRepository) {
        this.taskRepository = taskRepository;
        this.taskCategoryRepository = taskCategoryRepository;
    }

    @Override
    public Task createTask(TaskDto taskDto) {
        Task task = new Task();

        task.setTaskName(taskDto.getTaskName());
        task.setTaskDescription(taskDto.getTaskDescription());
        task.setDeadline(taskDto.getDeadline());

        if (taskDto.getTaskCategory() != null) {
            var existingCategory = this.taskCategoryRepository.findTaskCategoryByCategoryName(taskDto.getTaskCategory().getCategoryName());
            if (existingCategory.isPresent() && Objects.equals(existingCategory.get().getCategoryDescription(), taskDto.getTaskCategory().getCategoryDescription())) {
                task.setTaskCategory(existingCategory.get());
            } else {
                task.setTaskCategory(this.createTaskCategory(taskDto.getTaskCategory()));
            }
        }

        return this.taskRepository.save(task);
    }

    @Override
    public TaskDto updateTask(Long taskId, TaskDto taskDto) {
        Task toUpdate = this.taskRepository.findById(taskId).orElseThrow(EntityNotFoundException::new);

        toUpdate.setTaskName(taskDto.getTaskName());
        toUpdate.setTaskDescription(taskDto.getTaskDescription());
        toUpdate.setDeadline(taskDto.getDeadline());

        if (taskDto.getTaskCategory() == null) {
            toUpdate.setTaskCategory(null);
        } else if (toUpdate.getTaskCategory() == null
                || !Objects.equals(taskDto.getTaskCategory().getCategoryName(), toUpdate.getTaskCategory().getCategoryName())
                || !Objects.equals(taskDto.getTaskCategory().getCategoryDescription(), toUpdate.getTaskCategory().getCategoryDescription())) {
            var existingCategory = this.taskCategoryRepository.findTaskCategoryByCategoryName(taskDto.getTaskCategory().getCategoryName());
            if (existingCategory.isPresent()
                    && (taskDto.getTaskCategory().getCategoryDescription() == null
                    || Objects.equals(existingCategory.get().getCategoryDescription(), taskDto.getTaskCategory().getCategoryDescription()))) {
                toUpdate.setTaskCategory(existingCategory.get());
            } else {
                toUpdate.setTaskCategory(this.createTaskCategory(taskDto.getTaskCategory()));
            }
        }

        return TaskDto.fromTask(this.taskRepository.save(toUpdate));
    }

    @Override
    public void deleteTask(Long taskId) {
        this.taskRepository.deleteById(taskId);
    }

    @Override
    public List<TaskDto> getTasks() {
        return StreamSupport.stream(
                        this.taskRepository.findAll().spliterator(), false)
                .map(TaskDto::fromTask)
                .collect(Collectors.toList());
    }

    @Override
    public TaskDto getTask(Long taskId) {
        return TaskDto.fromTask(this.taskRepository.findById(taskId).orElseThrow(EntityNotFoundException::new));
    }

    @Override
    public TaskCategoryDto getTaskCategory(Long categoryId) {
        return TaskCategoryDto.fromTaskCategory(this.taskCategoryRepository.findById(categoryId).orElseThrow(EntityNotFoundException::new));
    }

    @Override
    public List<TaskCategoryDto> getTaskCategories() {
        return StreamSupport.stream(
                        this.taskCategoryRepository.findAll().spliterator(), false)
                .map(TaskCategoryDto::fromTaskCategory)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteTaskCategory(Long categoryId) {
        this.taskCategoryRepository.deleteById(categoryId);
    }

    @Override
    public TaskCategoryDto updateTaskCategory(Long categoryId, TaskCategoryDto taskCategoryDto) {
        TaskCategory toUpdate = this.taskCategoryRepository.findById(categoryId).orElseThrow(EntityNotFoundException::new);

        toUpdate.setCategoryName(taskCategoryDto.getCategoryName());
        toUpdate.setCategoryDescription(taskCategoryDto.getCategoryDescription());

        return TaskCategoryDto.fromTaskCategory(this.taskCategoryRepository.save(toUpdate));
    }

    @Override
    public TaskCategory createTaskCategory(TaskCategoryDto taskCategoryDto) {
        TaskCategory taskCategory = new TaskCategory();

        var existingCategory = this.taskCategoryRepository.findTaskCategoryByCategoryName(taskCategoryDto.getCategoryName());
        if (existingCategory.isPresent()) {
            throw new RuntimeException();
        }

        taskCategory.setCategoryName(taskCategoryDto.getCategoryName());
        taskCategory.setCategoryDescription(taskCategoryDto.getCategoryDescription());

        return this.taskCategoryRepository.save(taskCategory);
    }
}
