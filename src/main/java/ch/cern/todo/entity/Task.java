package ch.cern.todo.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "TASK")
public class Task {
    @Id
    @SequenceGenerator(name = "TaskSequence", sequenceName = "TASK_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TaskSequence")
    @Column(name = "TASK_ID", updatable = false, nullable = false)
    private Long taskId;
    @Column(name = "TASK_NAME")
    private String taskName;
    @Column(name = "TASK_DESCRIPTION")
    private String taskDescription;
    @Column(name = "DEADLINE")
    private Date deadline;
    @ManyToOne
    @JoinColumn(name = "CATEGORY_ID")
    private TaskCategory taskCategory;
}
